#' Root Mean Squared Error
#'
#' Compute the root mean squared error (RMSE).
#'
#' @param error numeric vector of the prediction errors.
#'
#' @return Numeric scalar, the root mean squared error.
#' @author Robert Müller
#' @export

rmse <- function(error) {
  sqrt(sum(error^2) / length(error))
}
