#' Fit Cluster Processes to a Tree Species Point Pattern
#'
#' Fit homogeneous cluster point process models to a spatial point pattern of a
#' tree species and test the models for goodness of fit. Models are fitted by
#' the method of minimum contrast using Ripley's K as summary statistic. The
#' goodness of fit (GOF) is assessed by the Diggle-Cressie-Loosmore-Ford (DCLF)
#' test based on the nearest neighbor distribution function G(r) and the empty
#' space function F(r). \cr
#' This function is designed for batch processing: Several types of cluster
#' models can be specified and each model type will be fit successively to the
#' point pattern. Additionally, several tuning parameters and optimization
#' algorithms for fitting can be specified. The function will fit each possible
#' combination of model type, tuning parameter and optimization algorithm to the
#' point pattern. \cr
#' Parallel computation is possible either by setting \code{nCores > 1} or by
#' providing an object of class \code{"cluster"} to the argument \code{cl}. Be
#' aware that this function might create objects that exceed the available
#' memory (especially if the input point pattern is large).
#'
#' @param species character. Species code listed in column \code{sp} of
#'   \code{trees} that will be used to extract the individuals of the species
#'   from \code{trees}.
#' @param trees data frame of locations of individuals. Typically an output of
#'   \code{\link{subsetBCIStem}}. The data frame should contain one row per
#'   individual and the following columns: \describe{ \item{sp}{code specifying
#'   the tree species.} \item{gx}{x-coordinate of tree location.}
#'   \item{gy}{y-coordinate of tree location.}}
#' @param plotdim numeric vector. Extension of rectangular plot, given as \code{
#'   c(<x_length>,<y_length>)}.
#' @param model character vector. Specify one or several cluster process
#'   model(s). See \code{clusters} of \code{kppm} for available options.
#' @param RMmodel character vector. Specify one or several covariance model(s).
#'   \code{RMmodel} is only used if a log-Gaussian Cox process (LGCP) is fitted
#'   (i.e. \code{model = "LGCP"}). See \code{kppm} for details on LGCP and how
#'   covariance model names should be specified.
#' @param nu numeric vector. The shape parameter, which is only used for the
#'   variance-gamma process (i.e. \code{model = "VarGamma"}) or log-Gausian Cox process
#'   with a Matérn covariance model (i.e. \code{model = "LGCP"} and \code{RMmodel =
#'   "matern"}).
#' @param tun.c numeric vector. Power transformation parameter used by the
#'   minimum contrast criterion. See functions \code{clusterfit} and
#'   \code{mincontrast} of \strong{spatstat} package, where \code{tun.c} is
#'   denoted by q. Diggle (2014) recommends to set \code{tun.c} to 0.25 or 0.5.
#' @param Dmax numeric vector. Maximum distance used by the minimum contrast
#'   criterion. See functions \code{clusterfit} and \code{mincontrast} of
#'   \strong{spatstat} package, where \code{tun.c} is denoted by \code{rmax}.
#'   Diggle (2014) recommends to try different \code{Dmax} values to tune
#'   fitting results.
#' @param algorithm character vector. The algorithm used for optimization. The
#'   argument algorithm is passed to \code{\link[stats]{optim}} as the argument
#'   method.
#' @param control a list of control parameters which are required by the
#'   optimization function \code{\link[stats]{optim}}. See there for details. Set the standard values
#'   by sending an empty list (i.e. \code{control = list()}).
#' @param nsim integer. Number of simulations used for the DCLF test.
#' @param mod logical. If \code{TRUE}, the fitted cluster point process models
#'   will be returned in a list. If \code{FALSE}, only a data frame containing
#'   the p-values of the DCLF test will be returned. Setting this argument to
#'   \code{FALSE} will reduce the amount of memory that is required to run this
#'   function.
#' @param nCores integer. Number of cores used for parallel computation. If not
#'   specified, \code{nCores} will be set to the maximum number of available
#'   cores. Avoid parallel computation by setting \code{nCores = 1} and \code{cl
#'   = NA}.
#' @param type character. Specify the type of cluster for parallel computing.
#'   Available options are \code{"PSOCK"} and \code{"FORK"}. See
#'   \code{\link[parallel]{makeCluster}} for details. If set to \code{NULL} an
#'   appropriate cluster type will be chosen based on your platform.
#' @param use_lb logical. If \code{TRUE}, load balancing is used for parallel
#'   computation.
#' @param cl an object of class \code{"cluster"} representing a parallel socket
#'   cluster. Only use this argument if you want to use parallel computation,
#'   but don't want this function to create the parallel cluster. Otherwise set
#'   this argument to \code{NA}.
#'
#' @return
#' A list with the following two elements:
#' \item{gf.pVal}{a data frame containing for each fitted cluster process model
#' the estimated parameters and the \eqn{p} values of the DCLF test. The data frame
#' contains the following columns:
#'   \describe{
#'     \item{species}{the species code of the point pattern to which the model
#'       was fitted.}
#'     \item{model}{the type of fitted cluster point process.}
#'     \item{algorithm}{the algorithm used for numerical optimization.}
#'     \item{tun.c}{the power transformation parameter of the minimum contrast criterion.}
#'     \item{Dmax}{the maximum distance of the minimum contrast criterion.}
#'     \item{nu}{the shape parameter. Only returned for a variance-gamma process
#'       (i.e. \code{model = "VarGamma"}) or a log-Gausian Cox process with a
#'       Matérn covariance model (i.e. \code{model = "LGCP"} and \code{RMmodel =
#'       "matern"}).}
#'     \item{RMmodel}{the covariance model. Only returned for log-Gaussian Cox
#'       process (i.e. \code{model = "LGCP"}).}
#'     \item{lambda}{estimated process intensity. This is equal to \code{mu *
#'       kappa.}}
#'     \item{mu}{estimated number of offspring points per cluster.}
#'     \item{kappa}{estimated parent point intensity.}
#'     \item{scale}{estimated scale parameter that determines the cluster size.
#'       The cluster processes differ in the way that cluster size is
#'       parameterized (see Baddeley et al. 2015):
#'       \describe{
#'         \item{Matérn cluster process}{\code{scale} is the radius \eqn{R} of a disc
#'           centered around the parent point.}
#'         \item{Thomas cluster process}{\code{scale} is the variance (\eqn{\sigma^2}) of the
#'           isotropic Gaussian distribution.}
#'         \item{Variance-gamma cluster process}{\code{scale} is the scale parameter
#'         (\eqn{\eta}) of the variance-gamma distribution.}
#'         \item{Cauchy cluster process}{\code{scale} is the squared scale parameter
#'         (\eqn{\omega^2}) of the bivariate Cauchy distribution.}
#'       }
#'     }
#'     \item{G.p}{\eqn{p} value of the DCLF test using the nearest neighbor
#'       distribution function as summary statistic.}
#'     \item{F.p}{\eqn{p} value of the DCLF test using the empty space function as
#'       summary statistic.}
#'   }
#' }
#' \item{modList}{a list containing the fitted cluster process models for each
#'   combination of cluster process, covariance model (if applicable), shape
#'   parameter (if applicable), optimization algorithm, tuning constant and
#'   maximum distance \code{Dmax}. Model names are composed as follows:
#'   \code{model_algorithm_tun.c_Dmax_nu_RMmodel}.}
#'
#' @author Robert Müller
#'
#' @seealso \code{\link{kppmFitGofAllSpecies}}
#'
#' @references \insertRef{Baddeley.et.al.2014}{ppartition}
#' @references \insertRef{baddeley_spatial_2015}{ppartition}
#' @references \insertRef{cressie_statistics_1993}{ppartition}
#' @references \insertRef{diggle_displaced_1986}{ppartition}
#' @references \insertRef{diggle_statistical_2014}{ppartition}
#' @references \insertRef{John.et.al.2007}{ppartition}
#' @references \insertRef{loosmore_statistical_2006}{ppartition}
#' @references \insertRef{Plotkin.et.al.2000}{ppartition}
#' @references \insertRef{ripley_second-order_1976}{ppartition}
#'
#' @importFrom pbapply pboptions pblapply
#' @import spatstat
#' @export

kppmFitGofSpecies <- function(species, trees, plotdim, model,
                              RMmodel = NA, nu = NA, tun.c = 0.25, Dmax,
                              algorithm = "Nelder-Mead", control = list(),
                              mod = TRUE, nsim = 199, cl = NA,
                              nCores = detectCores(), type = NULL,
                              use_lb = TRUE) {

  # print progress of computation to console
  cat(paste0("\n", "Species code: ", species, ", Start: ", Sys.time()))

  # prepare input values for batch processing
  if ("VarGamma" %in% model) {
    vgExist <- TRUE
    model <- model[model != "VarGamma"]
  }
  if ("LGCP" %in% model) {
    lgcpExist <- TRUE
    model <- model[model != "LGCP"]
  }

  methodDF <- expand.grid(
    tun.c = tun.c, Dmax = Dmax, nu = NA, RMmodel = NA,
    model = model, algorithm = algorithm,
    stringsAsFactors = FALSE
  )

  if (exists("vgExist")) {
    vgMethodDF <- expand.grid(
      tun.c = tun.c, Dmax = Dmax, nu = nu,
      RMmodel = NA, model = "VarGamma",
      algorithm = algorithm, stringsAsFactors = FALSE
    )
    methodDF <- rbind(methodDF, vgMethodDF)
  }

  if (exists("lgcpExist")) {
    if (any(c("whittle", "matern", "handcock") %in% RMmodel)) {
      RMNuModel <- RMmodel[RMmodel %in% c("whittle", "matern", "handcock")]
      lgcpMethodDFNu <- expand.grid(
        tun.c = tun.c, Dmax = Dmax, nu = nu,
        RMmodel = RMNuModel, model = "LGCP",
        algorithm = algorithm, stringsAsFactors = FALSE
      )
      methodDF <- rbind(methodDF, lgcpMethodDFNu)
      RMmodel <- RMmodel[!(RMmodel %in% c("whittle", "matern", "handcock"))]
    }
    if (length(RMmodel) > 0) {
      lgcpMethodDF <- expand.grid(
        tun.c = tun.c, Dmax = Dmax, nu = NA,
        RMmodel = RMmodel, model = "LGCP",
        algorithm = algorithm, stringsAsFactors = FALSE
      )
      methodDF <- rbind(methodDF, lgcpMethodDF)
    }
  }
  methodDF <- methodDF[
    order(
      methodDF$algorithm, methodDF$model, methodDF$RMmodel,
      methodDF$nu, methodDF$tun.c, methodDF$Dmax
    ),
    c("model", "algorithm", "tun.c", "Dmax", "nu", "RMmodel")
  ]
  methodList <- apply(methodDF, 1, as.list)
  methodList <- lapply(methodList, unlist)

  # create ppp object based on species point pattern, required for
  # following functions
  sp.ppp <- createPPP(species = species, trees = trees, plotdim = plotdim)

  # start cluster for parallel computing if nCores > 1 and socket cluster not yet started
  if (!is.null(cl)) {
    if (is.na(cl)[1]) {
      if (nCores == 1) {
        cl <- NULL
      } else {
        clLocal <- TRUE
        # set load balancing
        opb <- pboptions(use_lb = use_lb)

        if (is.null(type)) {
          if (.Platform$OS.type == "unix") {
            type <- "FORK"
          } else {
            type <- "PSOCK"
          }
        }

        cl <- parallel::makeCluster(nCores, type = type)
      }
    }
  }
  clType <- class(cl[[1]])[1]
  if (clType == "SOCKnode" & (exists("clLocal") | is.null(type))) {
    cat("\nStarting socket cluster and/or loading packages. This may take some time ...")
    parallel::clusterEvalQ(cl, library(spatstat))
  }


  # start fitting of species' point processes and goodness-of-fit testing
  cat(paste("\n", "Fitting point processes to species' point pattern and testing for goodness-of-fit ...", "\n"))
  gf.species <- pblapply(
    X = methodList, sp.ppp = sp.ppp, cl = cl, mod = mod,
    nsim = nsim, control = control,
    FUN = function(method, sp.ppp, nsim, control, mod) {
      tun.c <- as.numeric(method["tun.c"])
      Dmax <- as.numeric(method["Dmax"])
      nu <- as.numeric(method["nu"])

      modClust <- kppm(
        X = sp.ppp, trend = ~1, clusters = method["model"], method = "mincon",
        algorithm = method["algorithm"], statistic = "K", q = tun.c,
        statargs = list(correction = "Ripley", nlarge = Inf, rmax = Dmax),
        rmax = Dmax, control = control, nu = nu, model = method["RMmodel"]
      )

      breaks <- seq(0, Dmax, 0.5)

      gf.test.G <- dclf.test(
        X = modClust, fun = "Gest", nsim = nsim,
        r = breaks, correction = "km", alternative = "two.sided",
        rinterval = c(0, Dmax), verbose = FALSE,
        transform = expression(asin(sqrt(.)))
      )

      gf.test.F <- dclf.test(
        X = modClust, fun = "Fest", nsim = nsim,
        r = breaks, correction = "km", alternative = "two.sided",
        rinterval = c(0, Dmax), verbose = FALSE,
        transform = expression(asin(sqrt(.)))
      )

      parms <- modClust$par
      names(parms) <- c("kappa", "scale")

      gf.p <- c(
        lambda = modClust$lambda,
        mu = modClust$mu,
        parms,
        G.p = gf.test.G$p.value,
        F.p = gf.test.F$p.value
      )

      if (mod) {
        fitMod <- list(gf.p = gf.p, modClust = modClust)
      } else {
        fitMod <- list(gf.p = gf.p)
      }

      return(fitMod)
    }
  )

  # stop socket cluster if defined in local environment
  if (exists("clLocal")) {
    stopCluster(cl)
    if (use_lb) pboptions(opb)
  }

  # convert results to appropriate output format
  gf.parms <- t(sapply(gf.species, function(ppmod) ppmod$gf.p))

  gf.pVal <- cbind(methodDF, gf.parms)

  if (mod) {
    modList <- lapply(gf.species, function(x) x$modClust)
    names(modList) <- paste(methodDF$model, methodDF$algorithm, methodDF$tun.c,
      methodDF$Dmax, methodDF$nu, methodDF$RMmodel,
      sep = "_"
    )
    gfList <- list(gf.pVal = gf.pVal, modList = modList)
  } else {
    gfList <- gf.pVal
  }

  cat(paste("End:", Sys.time(), "\n"))

  return(gfList)
}
