#' Fit Cluster Processes to Point Patterns of Tree Species
#'
#' Fit homogeneous cluster point process models to spatial point patterns of
#' multiple tree species and test the models for goodness of fit. Models are
#' fitted by the method of minimum contrast using Ripley's K as summary
#' statistic. The goodness of fit (GOF) is assessed by the
#' Diggle-Cressie-Loosmore-Ford (DCLF) test based on the nearest neighbor
#' distribution function G(r) and the empty space function F(r). \cr This
#' function is designed for batch processing: Several types of cluster models
#' can be specified and each model type will be fit successively to the point
#' patterns. Additionally, several tuning parameters and optimization algorithms
#' for fitting can be specified. The function will fit each possible combination
#' of model type, tuning parameter and optimization algorithm to the point
#' pattern of each aggregated species (as indicated by \code{"A"} in column
#' \code{"A"} of the data frame \code{aggTest}). \cr Parallel computation is
#' possible by setting \code{nCores > 1}. Be aware that this function might
#' create objects that exceed the available memory (especially if the input
#' point patterns are large).
#'
#' @param aggTest data frame containing the result of an aggregation test.
#'   Typically the output of \code{\link{species.aggreg.test}}.
#' @param trees data frame of locations of individuals. Typically an output of
#'   \code{\link{subsetBCIStem}}. The data frame should contain one row per
#'   individual and the following columns: \describe{ \item{sp}{code specifying
#'   the tree species.} \item{gx}{x-coordinate of tree location.}
#'   \item{gy}{y-coordinate of tree location.}}
#' @param plotdim numeric vector. Extension of rectangular plot, given as \code{
#'   c(<x_length>,<y_length>)}.
#' @param model character vector. Specify one or several cluster process
#'   model(s). See \code{clusters} of \code{kppm} for available options.
#' @param RMmodel character vector. Specify one or several covariance model(s).
#'   \code{RMmodel} is only used if a log-Gaussian Cox process (LGCP) is fitted
#'   (i.e. \code{model = "LGCP"}). See \code{kppm} for details on LGCP and how
#'   covariance model names should be specified.
#' @param nu numeric vector. The shape parameter, which is only used for the
#'   variance-gamma process (i.e. \code{model = "VarGamma"}) or log-Gausian Cox process
#'   with a Matérn covariance model (i.e. \code{model = "LGCP"} and \code{RMmodel =
#'   "matern"}).
#' @param tun.c numeric vector. Power transformation parameter used by the
#'   minimum contrast criterion. See functions \code{clusterfit} and
#'   \code{mincontrast} of \strong{spatstat} package, where \code{tun.c} is
#'   denoted by q. Diggle (2014) recommends to set \code{tun.c} to 0.25 or 0.5.
#' @param Dmax numeric vector. Maximum distance used by the minimum contrast
#'   criterion. See functions \code{clusterfit} and \code{mincontrast} of
#'   \strong{spatstat} package, where \code{tun.c} is denoted by \code{rmax}.
#'   Diggle (2014) recommends to try different \code{Dmax} values to tune
#'   fitting results.
#' @param algorithm character vector. The algorithm used for optimization. The
#'   argument algorithm is passed to \code{\link[stats]{optim}} as the argument
#'   method.
#' @param control a list of control parameters which are required by the
#'   optimization function \code{\link[stats]{optim}}. See there for details. Set the standard values
#'   by sending an empty list (i.e. \code{control = list()}).
#' @param nsim integer. Number of simulations used for the DCLF test.
#' @param mod logical. If \code{TRUE}, the fitted cluster point process models
#'   will be returned in a list. If \code{FALSE}, only a data frame containing
#'   the p-values of the DCLF test will be returned. Setting this argument to
#'   \code{FALSE} will reduce the amount of memory that is required to run this
#'   function.
#' @param dir character. Path to directory where intermediate results will be
#'   saved. If \code{dir} is specified, fitted models will be saved in separate RDS
#'   files for each species within the directory. These files can be loaded into
#'   the R workspace with \code{\link[base]{readRDS}}.
#' @param nsim integer. Number of simulations used for the DCLF test.
#' @param combine logical. If \code{TRUE}, the data frames \code{gf.pVal} that
#'   contain the test result and the parameters of the fitted models for each
#'   species will be combined to a single data frame.
#' @param nCores integer. Number of cores used for parallel computation. If not
#'   specified, \code{nCores} will be set to the maximum number of available
#'   cores. Avoid parallel computation by setting \code{nCores = 1}.
#' @param type character. Specify the type of cluster for parallel computing.
#'   Available options are \code{"PSOCK"} and \code{"FORK"}. See
#'   \code{\link[parallel]{makeCluster}} for details. If set to \code{NULL} an
#'   appropriate cluster type will be chosen based on your platform.
#' @param use_lb logical. If \code{TRUE}, load balancing is used for parallel
#'   computation.
#'
#' @return
#' The structure of the returned object depends on the arguments \code{mod} and
#' \code{combine}.\cr
#'
#' If \code{combine = TRUE}: A list with the following two elements:
#'
#' \item{gf.pVal}{a data frame containing for each fitted cluster process model
#' the estimated parameters and the \eqn{p} values of the DCLF test. The data frame
#' contains the following columns:
#'   \describe{
#'     \item{species}{the species code of the point pattern to which the model
#'       was fitted.}
#'     \item{model}{the type of fitted cluster point process.}
#'     \item{algorithm}{the algorithm used for numerical optimization.}
#'     \item{tun.c}{the power transformation parameter of the minimum contrast criterion.}
#'     \item{Dmax}{the maximum distance of the minimum contrast criterion.}
#'     \item{nu}{the shape parameter. Only returned for a variance-gamma process
#'       (i.e. \code{model = "VarGamma"}) or a log-Gausian Cox process with a
#'       Matérn covariance model (i.e. \code{model = "LGCP"} and \code{RMmodel =
#'       "matern"}).}
#'     \item{RMmodel}{the covariance model. Only returned for log-Gaussian Cox
#'       process (i.e. \code{model = "LGCP"}).}
#'     \item{lambda}{estimated process intensity. This is equal to \code{mu *
#'       kappa.}}
#'     \item{mu}{estimated number of offspring points per cluster.}
#'     \item{kappa}{estimated parent point intensity.}
#'     \item{scale}{estimated scale parameter that determines the cluster size.
#'       The cluster processes differ in the way that cluster size is
#'       parameterized (see Baddeley et al. 2015):
#'       \describe{
#'         \item{Matérn cluster process}{\code{scale} is the radius \eqn{R} of a disc
#'           centered around the parent point.}
#'         \item{Thomas cluster process}{\code{scale} is the variance (\eqn{\sigma^2}) of the
#'           isotropic Gaussian distribution.}
#'         \item{Variance-gamma cluster process}{\code{scale} is the scale parameter
#'         (\eqn{\eta}) of the variance-gamma distribution.}
#'         \item{Cauchy cluster process}{\code{scale} is the squared scale parameter
#'         (\eqn{\omega^2}) of the bivariate Cauchy distribution.}
#'       }
#'     }
#'     \item{G.p}{\eqn{p} value of the DCLF test using the nearest neighbor
#'       distribution function as summary statistic.}
#'     \item{F.p}{\eqn{p} value of the DCLF test using the empty space function as
#'       summary statistic.}
#'   }
#' }
#' \item{modList}{a list containing the fitted cluster process models for each
#'   combination of cluster process, covariance model (if applicable), shape
#'   parameter (if applicable), optimization algorithm, tuning constant and
#'   maximum distance \code{Dmax}. Model names are composed as follows:
#'   \code{model_algorithm_tun.c_Dmax_nu_RMmodel}.}
#'
#' If \code{combine = FALSE}: A list holding the result for each species in a
#' sublist. Each sublist contains the fitted point process models in the list
#' \code{modList} and the result of the DCLF tests in the data frame
#' \code{gf.pVal}. See above for details about the objects \code{modList} and
#' \code{gf.pVal}.
#'
#' Setting \code{mod = FALSE} will remove the \code{modList} object(s) from the
#' returned result.
#'
#' @author Robert Müller
#'
#' @seealso \code{\link{kppmFitGofSpecies}}
#'
#' @references \insertRef{Baddeley.et.al.2014}{ppartition}
#' @references \insertRef{baddeley_spatial_2015}{ppartition}
#' @references \insertRef{cressie_statistics_1993}{ppartition}
#' @references \insertRef{diggle_displaced_1986}{ppartition}
#' @references \insertRef{diggle_statistical_2014}{ppartition}
#' @references \insertRef{John.et.al.2007}{ppartition}
#' @references \insertRef{loosmore_statistical_2006}{ppartition}
#' @references \insertRef{Plotkin.et.al.2000}{ppartition}
#' @references \insertRef{ripley_second-order_1976}{ppartition}
#'
#' @importFrom pbapply pboptions
#' @export

kppmFitGofAllSpecies <- function(aggTest, trees, plotdim, model,
                                 RMmodel = NA, nu = NA, tun.c, Dmax,
                                 algorithm = "Nelder-Mead", control = list(),
                                 mod = TRUE, dir = NULL, nsim = 199,
                                 combine = TRUE, nCores = detectCores(),
                                 type = NULL, use_lb = TRUE) {

  splist <- rownames(aggTest)[aggTest$A == "A"]

  # start cluster for parallel computing if nCores > 1
  if (nCores == 1) {
    cl <- NULL
  } else {

    # set load balancing
    opb <- pboptions(use_lb = use_lb)

    if (is.null(type)) {
      if (.Platform$OS.type == "unix") {
        type <- "FORK"
      } else {
        type <- "PSOCK"
      }
    }

    cl <- parallel::makeCluster(nCores, type = type)

    if (type == "PSOCK") {
      cat("\nStarting socket cluster and loading packages. This may take some time ...")
      parallel::clusterEvalQ(cl, library(spatstat))
    }
  }

  gf.test <- sapply(
    X = splist, simplify = FALSE, USE.NAMES = TRUE,
    cl = cl, dir = dir, trees = trees, tun.c = tun.c, plotdim = plotdim, model = model,
    RMmodel = RMmodel, nu = nu, Dmax = Dmax, algorithm = algorithm, control = control,
    nsim = nsim, mod = mod, type = type,
    FUN = function(species, dir, trees, tun.c, plotdim, model,
                   RMmodel, nu, Dmax, algorithm, control,
                   nsim, mod, cl, type) {

      # start fitting of species' point processes and goodness-of-fit testing
      gfList <- kppmFitGofSpecies(
        species = species, trees = trees, tun.c = tun.c, plotdim = plotdim, model = model,
        RMmodel = RMmodel, nu = nu, Dmax = Dmax, algorithm = algorithm, control = control,
        nsim = nsim, mod = mod, cl = cl, type = type
      )

      # save intermediate result to disk
      if (!is.null(dir)) saveRDS(gfList, paste0(dir, "/", species, ".rds"))

      return(gfList)
    }
  )

  if (!is.null(cl)) {
    stopCluster(cl)
    if (use_lb) pboptions(opb)
  }

  # if combine == TRUE: combine data frames containing test results to a single
  # data frame

  if (combine) {
    if (mod) {
      gf.pVal <- lapply(names(gf.test), function(species) {
        data.frame(species = species, gf.test[[species]]$gf.pVal)
      })
      gf.pVal <- Reduce(rbind, gf.pVal)
      modList <- lapply(gf.test, function(species) species$modList)
      gf.test <- list(gf.pVal = gf.pVal, modList = modList)
    } else {
      gf.test <- lapply(names(gf.test), function(species) {
        data.frame(species = species, gf.test[[species]])
      })
      gf.test <- Reduce(rbind, gf.test)
    }
  }

  return(gf.test)
}
