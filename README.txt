#' # ppartition - Test for Associations between Plant Species and Soil Nutrients
#'
#'
#' ### Description
#'
#' This README file describes the R package **ppartition** that is supplement to
#' the journal article '[Evidence for Soil Phosphorus Resource Partitioning in a
#' Diverse Tropical Tree Community](https://doi.org/10.3390/f15020361)' by Robert
#' Müller, Helmut Elsenbeer and Benjamin L. Turner. The package contains the R
#' functions, the data and a top-level R script required to reproduce the
#' analyses of the publication.
#'
#' ### Package Authors
#'
#' 1. Corresponding author and package maintainer:
#'    * Name: Robert Müller
#'    * Affiliation: Geological Survey, State Office for Mining, Geology and Raw
#'                   Materials of Brandenburg, Inselstraße 26, 03046 Cottbus, Germany
#'    * ORCID: [0009-0004-4191-5511](https://www.orcid.org/0009-0004-4191-5511)
#'
#' 2. Co-author:
#'    * Name: Helmut Elsenbeer
#'    * Affiliation: Institute of Environmental Science and Geography,
#'                   University of Potsdam, Karl-Liebknecht-Str. 24-25,
#'                   14476 Potsdam, Germany
#'    * ORCID: [0000-0002-5033-7539](https://www.orcid.org/0000-0002-5033-7539)
#'
#' 3. Co-author:
#'    * Name: Benjamin L. Turner
#'    * Affiliation: Institute of Agriculture and Life Sciences, Gyeongsang
#'                   National University, Jinju 52828, Republic of Korea
#'    * ORCID: [0000-0002-6585-0722](https://www.orcid.org/0000-0002-6585-0722)
#'
#' 4. Co-author:
#'    * Name: Robert John
#'    * Affiliation: Biological Sciences, Indian Institute of Science Education
#'                   and Research Kolkata, Mohanpur 741246, West Bengal, India
#'    * ORCID: [0000-0002-2848-9413](https://www.orcid.org/0000-0002-2848-9413)
#'
#' 5. Contributing author:
#'    * Name: James W. Dalling
#'    * Affiliation: Department of Plant Biology, University of Illinois,
#'                   Urbana, Illinois 61801, USA
#'    * ORCID: [0000-0002-6488-9895](https://www.orcid.org/0000-0002-6488-9895)
#'
#' 6. Contributing author:
#'    * Name: Kyle E. Harms
#'    * Affiliation:  Department of Biological Sciences, Louisiana State
#'                    University, Baton Rouge, Louisiana 70803, USA
#'    * ORCID: [0000-0002-8842-382X](https://www.orcid.org/0000-0002-8842-382X)
#'
#' The authors 1, 2 and 3 wrote the package and the majority of the functions.
#' The authors 4, 5 and 6 contributed R functions to the package. Two
#' functions of the **spatstat** R package are included in this package.
#' Authorship of each function is specified in the function source files in
#' directory *R* and in the *DESCRIPTION* file.
#'
#' ### File and Directory Description
#'
#' *[analysis-ppartition.R](./analysis-ppartition.R)* - top-level R script
#' containing code to install the package and reproduce the analysis of the
#' publication.
#'
#' *[analysis-ppartition.html](https://robert_mueller.gitlab.io/ppartition/analysis-ppartition)* -
#' HTML version of the top-level R script *analysis-ppartition.R* that
#' contains the code to install the package and reproduce the analysis of the
#' publication. The HTML document is a human-readable version of the R script
#' that can be displayed in any web browser.
#'
#' *[data](./data)* – contains raw data and the results of the statistical
#' analyses.
#'
#' *[DESCRIPTION](./DESCRIPTION)* - standard R package file containing metadata
#' of the ppartition package.
#'
#' *[figures](./figures)* - contains the code to create the figures of the
#' manuscript and the supporting information of the publication.
#'
#' *[inst](./inst)* - contains the references that are displayed on the help
#' pages of the R functions.
#'
#' *[LICENSE](./LICENSE)* - contains details about the package licenses.
#'
#' *[LICENSE.md](./LICENSE.md)* - contains the full text of the major license
#' of the package.
#'
#' *[man](./man)* - contains the documentation files of the R functions in the
#' 'R documentation' format. Each documentation file is named by the associated
#' function. These files will be compiled to R function help pages during
#' installation of the package.
#'
#' *[manual.pdf](./manual.pdf)* - contains the manual of the **ppartition**
#' package. The manual is a complete documentation of all R functions and data
#' sets contained in the **ppartition** package.
#'
#' *[NAMESPACE](./NAMESPACE)* - standard R package file containing information
#' about functions imported from other R packages.
#'
#' *[R](./R)* - contains the R source code of the functions of the package. Each
#' R source file is named by the associated function.
#'
#' *[README.md](./README.md)* - this README file in Markdown format.
#'
#' *[README.txt](./README.txt)* - this README file in plain text format.
#'
#' *[README.pdf](./README.pdf)* - this README file in PDF format.
#'
#' *[tables](./tables)* - contains the code to create the tables of the
#' manuscript and the supporting information of the publication.
#'
#' *[.gitlab-ci.yml](./.gitlab-ci.yml)* - YAML file required to render the HTML
#' version of the data analysis report *analysis-ppartition.html* on GitLab.
#'
#' ### Acknowledgments
#'
#' We thank Robert John, James W. Dalling and Kyle E. Harms who provided the
#' source code of several functions that are now part of this package. Their
#' work was supported by the Soils Initiative of the Smithsonian Tropical
#' Research Institute and the US National Science Foundation grants DEB 0211004,
#' DEB 0211115, DEB 0212284, DEB 0212818, and OISE 0314581.
#'
#' ### How to Cite This Material
#'
#' Müller, R.; Elsenbeer, H.; Turner, B.L.; John, R. 2024. ppartition: Test for
#' Associations between Plant Species and Soil Nutrients. R Package
#' Version 1.0.0. Zenodo. https://doi.org/10.5281/zenodo.10525337.
#'
#' ### This Material is Supplement to
#'
#' Müller, R.; Elsenbeer, H.; Turner, B.L. 2024. Evidence for Soil Phosphorus
#' Resource Partitioning in a Diverse Tropical Tree Community. Forests, 15(2),
#' 361. https://doi.org/10.3390/f15020361.
