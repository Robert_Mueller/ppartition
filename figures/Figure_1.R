# This script contains the code that was used to create figure 1 of the paper
# "Evidence for Soil Phosphorus Resource Partitioning in a Diverse Tropical
# Tree Community"

# Load the required packages.
library(sf)
library(osmdata)
library(grid)
library(tmap)

# Download the polygons of the Barro Colorado Nature Monument (BCNM), the Barro
# Colorado Island (BCI) 50-ha plot and the Panamanian provinces. The data sets
# are hosted by the Smithsonian Tropical Research Institute (STRI) and will be
# downloaded from the STRI GIS Data Portal. Be sure that you agree with the
# terms of use of the repository before you use the function to download the
# data set.

# Download polygons of Barro Colorado Nature Monument.
# Link to Metadata: https://stridata-si.opendata.arcgis.com/datasets/SI::barro-colorado-nature-monument-boundaries/about
bcnm <- st_read(
  "https://services2.arcgis.com/HRY6x8qt5qjGnAA9/arcgis/rest/services/Barro_Colorado_Nature_Monument_Land/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson",
  quiet = TRUE
)
# Download polygon of BCI 50-ha plot.
# Link to Metadata: https://stridata-si.opendata.arcgis.com/datasets/SI::bci-plot-50ha/about
bci50ha <- st_read(
  "https://services2.arcgis.com/HRY6x8qt5qjGnAA9/arcgis/rest/services/BCI_Plot_50ha/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson",
  quiet = TRUE
)
# Download polygons of Panamanian provinces.
# Link to Metadata: https://stridata-si.opendata.arcgis.com/datasets/SI::panama-province-boundaries-2022/about
panama_provinces <- st_read(
  "https://services2.arcgis.com/HRY6x8qt5qjGnAA9/arcgis/rest/services/Panama_Province_Boundaries_2022/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson",
  quiet = TRUE
)

# Download vector data from OpenStreetMap for a feature set with key=nature and
# value=water within bounding box [Xmin = -79.9°, Ymin = 9.1°, Xmax = -79.8°,
# Ymax = 9.2°] (EPSG=4326). Be sure that you agree with the terms of use for
# OpenStreetMap data (https://www.openstreetmap.org/copyright) before you use
# the function to download the data set.
imap_bbox <- c(xmin = -79.9, ymin = 9.1, xmax = -79.8, ymax = 9.2)
imap_bbox_poly <- imap_bbox %>%
  st_bbox(crs = 4326) %>%
  st_as_sfc()
imap_osm_water <- imap_bbox %>%
  opq(datetime = "2023-12-31T12:00:00Z") %>%
  add_osm_feature(key = "natural", value = "water") %>%
  osmdata_sf() %>%
  {
    rbind(sf:::select.sf(.$osm_polygons, osm_id), sf:::select.sf(.$osm_multipolygons, osm_id))
  } %>%
  st_cast() %>%
  st_transform(4326) %>%
  st_intersection(imap_bbox_poly)
imap_centroid <- st_centroid(imap_bbox_poly)

# Combine all Panamanian provinces to generate a polygon of the Panamanian state.
panama <- st_union(panama_provinces)

# Filter the BCNM data set for the BCI polygon.
bci <- subset(bcnm, NOMBRE == "Isla Barro Colorado")

# Create the figure. After executing the first line, a dialog box will appear.
# Specify a file name and directory where the figure will be saved. Afterwards,
# execute the remaining lines of code to finish the figure generation process.
jpeg(file.choose(new = TRUE), height = 3500, width = 3955, units = "px",
     res = 800, pointsize = 10)
tm_shape(imap_osm_water) +
  tm_polygons(col = "lightskyblue", border.col = "lightskyblue") +
  tm_shape(bci) +
  tm_borders(col = "red", lwd = 3) +
  tm_shape(bci50ha) +
  tm_polygons(col = "black") +
  tm_layout(bg.color = "lightgreen", inner.margins = 0) +
  tm_graticules(lines = FALSE, labels.size = 0.7) +
  tm_scale_bar(breaks = c(0, 1, 2, 3), text.size = 0.7)
omap <- tm_shape(panama) + tm_polygons() +
  tm_shape(imap_centroid) + tm_symbols(shape = 17, col = "black")
print(omap, vp = viewport(x = 0.369, y = 0.168, width = 0.5, height = 0.5))
dev.off()
